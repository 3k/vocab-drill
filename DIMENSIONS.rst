# vim: set filetype=rst :

===================  ==========  ============   =======  ================  ==============
DEVICE               RESOLUTION  ASPECT RATIO   DENSITY  PHYSICAL SIZE     QUALIFICATIONS
===================  ==========  ============   =======  ================  ==============
HTC One X            1280×720px  16:9 (WXVGA)   312 ppi  4.7˝ / 120 mm     large-xhdpi
Nexus 7              1280×800px  16:10 (WXVGA)  216 ppi  7˝ / 180 mm       large-hdpi
Xperia X10 mini pro  240×320px   4:3 (QVGA)     157 ppi  2.55˝ / 64.77 mm  small-mdpi
===================  ==========  =============  =======  ================  ==============

