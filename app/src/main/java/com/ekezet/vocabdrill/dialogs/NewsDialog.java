package com.ekezet.vocabdrill.dialogs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.helpers.AssetsUtils;

public class NewsDialog extends Activity
{
	private int mLimit = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_news);

		Intent i = getIntent();
		if (i != null)
			mLimit = i.getIntExtra("limit", mLimit);

		HashMap<String, String> messages = null;
		try
		{
			messages = getMessages();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			finish();
			return;
		}

		List<String> versions = new ArrayList<String>();
		versions.addAll(messages.keySet());
		Collections.sort(versions);
		Collections.reverse(versions);

		if (mLimit != 0)
		{
			List<String> tmp = new ArrayList<String>();
			for (int n = 0, N = versions.size(); n < mLimit && n < N; n++)
				tmp.add(versions.get(n));
			versions = tmp;
		}

		String txt = "";
		String ver = "";
		for (int n = 0, N = versions.size(); n < N; n++)
		{
			ver = versions.get(n);
			txt += String.format("<h2>v%s<h2><p>%s</p>", ver, messages.get(ver));
		}

		TextView tv = (TextView) findViewById(R.id.text);
		tv.setText(Html.fromHtml(txt));
	}

	private HashMap<String, String> getMessages() throws IOException
	{
		HashMap<String, String> msgs = new HashMap<String, String>();
		String[] files = AssetsUtils.listFiles(this, "news");
		for (int n = 0, N = files.length; n < N; n++)
			msgs.put(files[n].replace(".html", ""), AssetsUtils.readText(this, "news/" + files[n]));
		return msgs;
	}
}
