package com.ekezet.vocabdrill;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import com.ekezet.vocabdrill.helpers.DbxTools;
import com.ekezet.vocabdrill.helpers.Gradient;
import com.ekezet.vocabdrill.kvtml.Kvtml;
import com.ekezet.vocabdrill.kvtml.Kvtml.Identifier;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Iterator;

@SuppressWarnings("LoopStatementThatDoesntLoop")
public final class Config
{
	/**
	 * Name of preferences file.
	 */
	public final static String PREFS_FILE = "Config";
	public final static String PREF_RECENT_FILE = "recent_%d";
	public final static String STARTTIME_FILE = "STARTTIME";

	public final static String EXT_STORAGE_ALIAS = "/<Storage>";
	public final static String CACHE_DIR_ALIAS = "/<Cache>";

	public final static String PROGRESS_FILE = "Vocabdrill.kvtml";

	/**
	 * Interval date format for the Leitner-System.
	 *
	 * Month:Day:Hour
	 *
	 * Supported by Parley by the KDE Educational Project.
	 */
	public final static String INTERVAL_DATE_FORMAT = "MM:dd:HH";

	/**
	 * String delimiter used by concatenation routines.
	 */
	public final static String DELIM = ",";

	public static int maxRecentFilesNum = 20;

	/**
	 * Handle to the currently used KVTMl file.
	 */
	public static File inputFile;

	/**
	 * Previously loaded KVTML data.
	 */
	public static Kvtml lastData;

	/**
	 * Maximum number of choices in multiple choice mode.
	 */
	public static int choiceNumber = 4;

	/**
	 * Fill the list  with already answered questions.
	 */
	public static boolean forceChoiceNumber = true;

	/**
	 * True if the language of the question should be randomized during the test.
	 */
	public static boolean randomize = false;

	/**
	 * Time to sleep before jumping to the next question.
	 */
	public static long questionDelay = 300;

	/**
	 * If true the file browser will show the number of lesson files in
	 * directories.
	 */
	public static boolean scanDirectories = true;

	public static boolean confirmExit = true;
	public static boolean confirmRestart = true;
	public static boolean confirmProgress = false;

	private static String sQuestionLanguageId;
	private static String sAnswerLanguageId;

	/**
	 * Standard practice intervals for the Leitner System.
	 */
	public final static String[] defaultPracticeIntervals = {
		"00:00:12", "00:01:00", "00:03:00",
		"00:07:00", "00:14:00", "01:00:00",
		"02:00:00" };

	public static String[] practiceIntervals = {
		"00:00:12", "00:01:00", "00:03:00",
		"00:07:00", "00:14:00", "01:00:00",
		"02:00:00" };

	public static String[] recentFilesList;

	private static SharedPreferences sPrefs;
	private static File sCacheDir;
	private static File sDbxCacheDir;
	private static String sVersionName;
	private static int sVersionCode = -1;

	private static boolean sMuteNewsDialog = false;
	private static boolean sMuteDropboxSuccessDialog = false;
	private static int sPreviousVersionCode = 0;
	private static String sPreviousVersionName = "";

	/**
	 * Loads the configuration from the preferences file.
	 */
	public static SharedPreferences load(Context context)
	{
		sVersionCode = -1;
		try
		{
			setStartupTime(context);
			final PackageManager pm = context.getPackageManager();
			final PackageInfo inf = pm.getPackageInfo(context.getPackageName(), 0);
			sVersionName = inf.versionName;
			sVersionCode = inf.versionCode;
		}
		catch (NameNotFoundException e)
		{
			sVersionName = "?.?";
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		sCacheDir = context.getCacheDir();
		sCacheDir.mkdir();
		sDbxCacheDir = new File(sCacheDir, "/dropbox");
		sDbxCacheDir.mkdir();

		sPrefs = context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);

		sMuteNewsDialog = sPrefs.getBoolean("muteNewsDialog", sMuteNewsDialog);
		sPreviousVersionCode = sPrefs.getInt("previousVersionCode", sPreviousVersionCode);
		sPreviousVersionName = sPrefs.getString("previousVersionName", sPreviousVersionName);
		sMuteDropboxSuccessDialog = sPrefs.getBoolean("muteDropboxSuccessDialog", sMuteDropboxSuccessDialog);

		choiceNumber = sPrefs.getInt("choiceNumber", choiceNumber);
		forceChoiceNumber = sPrefs.getBoolean("forceChoiceNumber", forceChoiceNumber);
		// XXX: conversion necessarry for backwards compatibility
		questionDelay = Long.parseLong(sPrefs.getString("questionDelay", String.valueOf(questionDelay)));
		confirmExit = 1 == sPrefs.getInt("confirmExit", confirmExit ? 1 : 0);
		confirmRestart = 1 == sPrefs.getInt("confirmRestart", confirmRestart ? 1 : 0);
		confirmProgress = 1 == sPrefs.getInt("confirmProgress", confirmProgress ? 1 : 0);
		scanDirectories = 1 == sPrefs.getInt("scanDirectories", scanDirectories ? 1 : 0);
		maxRecentFilesNum = sPrefs.getInt("maxRecentFilesNum", maxRecentFilesNum);
		String load = sPrefs.getString("practiceIntervals", null);
		if (load != null)
		{
			// ensure we have 7 values
			String[] tmp = new String[defaultPracticeIntervals.length];
			String[] tmp2 = load.split(DELIM);
			for (int n = 0, N = tmp.length, N2 = tmp2.length; n < N; n++)
				if (n < N2)
					tmp[n] = tmp2[n];
				else
					tmp[n] = defaultPracticeIntervals[n];
			practiceIntervals = tmp;
		}

		DbxTools.initialize();
		loadRecentFilesList();
		Gradient.buildCache(16);
		return sPrefs;
	}

	/**
	 * Saves the configuration to the preferences file.
	 */
	public static boolean save()
	{
		SharedPreferences.Editor edit = sPrefs.edit();

		edit.putInt("choiceNumber", choiceNumber);
		edit.putBoolean("forceChoiceNumber", forceChoiceNumber);
		edit.putInt("confirmExit", confirmExit ? 1 : 0);
		edit.putInt("confirmProgress", confirmProgress ? 1 : 0);
		edit.putInt("confirmRestart", confirmRestart ? 1 : 0);
		edit.putInt("scanDirectories", scanDirectories ? 1 : 0);
		edit.putInt("maxRecentFilesNum", maxRecentFilesNum);
		edit.putBoolean("muteDropboxSuccessDialog", sMuteDropboxSuccessDialog);
		edit.putBoolean("muteNewsDialog", sMuteNewsDialog);
		// XXX: conversion necessarry for backwards compatibility
		edit.putString("questionDelay", String.valueOf(questionDelay));
		// generate array string for storage
		String store = "";
		for (int i = 0; i < 7; i++)
		{
			store += practiceIntervals[i];
			if (i < 6)
				store += DELIM;
		}
		store = store.substring(0, store.length() - 1);
		edit.putString("practiceIntervals", store);
		edit.putInt("previousVersionCode", sPreviousVersionCode);
		edit.putString("previousVersionName", sPreviousVersionName);
		return edit.commit();
	}

	/**
	 * Swaps questions/answer languages.
	 */
	public static void swapLanguages()
	{
		String s = sQuestionLanguageId;
		sQuestionLanguageId = sAnswerLanguageId;
		sAnswerLanguageId = s;
	}

	/**
	 * Returns the actual language id of the question.
	 */
	public static String getQuestionLangId(HashMap<String, Identifier> identifiers)
	{
		if (identifiers == null)
			return null;

		// return the value if it's already had been set
		if (sQuestionLanguageId != null)
			return sQuestionLanguageId;

		// get the first identifier from kvtml and set the value
		Iterator<String> it = identifiers.keySet().iterator();
		String answer = "";
		try
		{
			while (it.hasNext())
			{
				sQuestionLanguageId = String.valueOf(it.next());
				answer = String.valueOf(it.next());
				break;
			}
		} catch(Exception e)
		{
			e.printStackTrace();
			return sQuestionLanguageId;
		}
		sAnswerLanguageId = answer;
		return sQuestionLanguageId;
	}

	public static String getQuestionLangId()
	{
		if (lastData == null)
			return null;
		return getQuestionLangId(lastData.identifiers);
	}

	public static String getAnswerLangId()
	{
		return sAnswerLanguageId;
	}

	/**
	 * Set the id of the language of the questions and updates the language of the
	 * answer accordingly.
	 */
	public static void setQuestionLangId(String id)
	{
		sQuestionLanguageId = id;
		if (!sQuestionLanguageId.equals(sAnswerLanguageId))
		{
			return;
		}
		Object[] keys = lastData.identifiers.keySet().toArray();
		for (Object key : keys)
		{
			if (!String.valueOf(key).equals(id))
			{
				sAnswerLanguageId = String.valueOf(key);
				break;
			}
		}
	}

	/**
	 * Set the id of the language of the answer and updates the language of the
	 * questions accordingly.
	 */
	public static void setAnswerLangId(String id)
	{
		sAnswerLanguageId = id;
		if (!sQuestionLanguageId.equals(sAnswerLanguageId))
		{
			return;
		}
		Object[] keys = lastData.identifiers.keySet().toArray();
		for (Object key : keys)
		{
			if (!String.valueOf(key).equals(id))
			{
				sQuestionLanguageId = String.valueOf(key);
				break;
			}
		}
	}

	/**
	 * Loads and returns the recent files' list and removes paths that don't
	 * exist.
	 *
	 * @return True on success
	 */
	@SuppressLint("DefaultLocale")
	public static boolean loadRecentFilesList()
	{
		SharedPreferences.Editor edit = sPrefs.edit();
		String path;
		String key;
		recentFilesList = new String[maxRecentFilesNum];
		int i = 0;
		for (int n = 0; n < maxRecentFilesNum; n++)
		{
			key = String.format(PREF_RECENT_FILE, n);
			path = sPrefs.getString(key, null);
			if (path == null)
				edit.putString(key, "");
			else if ((new File(path)).exists())
			{
				Log.d(Config.class.getSimpleName(), String.format("recent[%d]: '%s'", i, path));
				recentFilesList[i] = path;
				i++;
			}
		}
		// TODO: test me: n = 0 || n = i
		for (int n = i; n < maxRecentFilesNum; n++)
			if (recentFilesList[n] == null)
				recentFilesList[n] = "";
		return edit.commit();
	}

	/**
	 * Inserts a path on top of the recent files' list.
	 */
	public static boolean insertRecentFile(String path)
	{
		if (recentFilesList == null || recentFilesList.length == 0 || path == null)
			return false;
		int mn = recentFilesList.length;
		String[] tmpList = new String[mn];
		int i = 0;
        for (String aRecentFilesList : recentFilesList)
        {
            if (!aRecentFilesList.equals(path))
            {
                tmpList[i] = aRecentFilesList;
                i++;
            }
        }
		recentFilesList = new String[mn];
		recentFilesList[0] = path;
		for (int n = 1; n < mn; n++)
			recentFilesList[n] = tmpList[n - 1] != null ? tmpList[n - 1] : "";
		return true;
	}

	public static void removeRecentFile(String path)
	{
		if (recentFilesList == null || recentFilesList.length == 0 || path == null)
			return;
		int mn = recentFilesList.length;
		String[] tmpList = new String[mn - 1];
		int i = 0;
        for (String aRecentFilesList : recentFilesList)
        {
            if (!aRecentFilesList.equals(path))
            {
                tmpList[i] = aRecentFilesList;
                i++;
            }
        }
		recentFilesList = tmpList;
	}

	@SuppressLint("DefaultLocale")
	public static boolean clearRecentFilesList()
	{
		SharedPreferences.Editor edit = sPrefs.edit();
		recentFilesList = new String[maxRecentFilesNum];
		String key;
		try
		{
			for (int n = 0; n < maxRecentFilesNum; n++)
			{
				key = String.format(PREF_RECENT_FILE, n);
				edit.putString(key, "");
				recentFilesList[n] = "";
			}
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			return edit.commit();
		}
		return edit.commit();
	}

	@SuppressLint("DefaultLocale")
	public static boolean saveRecentFilesList()
	{
		if (!hasRecentFiles())
			return false;
		SharedPreferences.Editor edit = sPrefs.edit();
		for (int n = 0, mn = recentFilesList.length; n < mn; n++)
			edit.putString(String.format(PREF_RECENT_FILE, n), recentFilesList[n]);
		return edit.commit();
	}

	public static boolean hasRecentFiles()
	{
		return recentFilesList != null && 0 < recentFilesList.length
			&& recentFilesList[0] != null && 0 < recentFilesList[0].length();
	}

	public static File getDbxCacheDir()
	{
		return sDbxCacheDir;
	}

	public static boolean isCacheDirEmpty()
	{
		return isDbxCacheDirEmpty();
	}

	public static boolean isDbxCacheDirEmpty()
	{
		File[] files = sDbxCacheDir.listFiles();
		return files == null || 0 == files.length;
	}

	public static boolean deleteDir(File dir)
    {
        if (dir != null && dir.isDirectory())
        {
            String[] children = dir.list();
            for (String aChildren : children)
            {
                boolean success = deleteDir(new File(dir, aChildren));
                if (!success)
                {
                    return false;
                }
            }
        }
        return dir != null && dir.delete();
    }

	public static boolean clearCacheDir()
	{
		deleteDir(sCacheDir);
		sDbxCacheDir.mkdirs();
		loadRecentFilesList();
		return true;
	}

	public static String getVersionName()
	{
		return sVersionName;
	}

	public static int getVersionCode()
	{
		return sVersionCode;
	}

	public static void setDropboxSuccessDialogMuted(boolean muted)
	{
		sMuteDropboxSuccessDialog = muted;
	}

	public static boolean isDropboxSuccessDialogMuted()
	{
		return sMuteDropboxSuccessDialog;
	}

	public static boolean setStartupTime(Context context) throws IOException
	{
		long ts;
		boolean ret = true;
		// installation identification file
		File iidf = new File(context.getFilesDir(), STARTTIME_FILE);
		try
		{
			if (!iidf.exists())
			{
				ts = System.currentTimeMillis();
				FileOutputStream fos = new FileOutputStream(iidf);
				ObjectOutputStream out = new ObjectOutputStream(fos);
				out.writeLong(ts);
				out.close();
			} else
			{
				FileInputStream fin = new FileInputStream(iidf);
				ObjectInputStream in = new ObjectInputStream(fin);
				in.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
			ret = false;
		}
		catch (NumberFormatException e)
		{
			Log.w(Config.class.getSimpleName(), String.format("Invalid STARTTIME. Deleting file: %s", iidf.getAbsolutePath()));
			e.printStackTrace();
			iidf.delete();
			ret = false;
		}
		return ret;
	}

	public static SharedPreferences getPrefs()
	{
		return sPrefs;
	}

	public static int getPreviousVersionCode()
	{
		return sPreviousVersionCode;
	}

	public static String getPreviousVersionName()
	{
		return sPreviousVersionName;
	}

	public static void invalidatePreviouseVersionCode()
	{
		sPreviousVersionCode = getVersionCode();
	}

	public static void invalidatePreviouseVersionName()
	{
		sPreviousVersionName = getVersionName();
	}
}