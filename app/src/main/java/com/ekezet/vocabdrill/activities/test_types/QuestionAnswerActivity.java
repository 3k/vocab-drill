package com.ekezet.vocabdrill.activities.test_types;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.kvtml.Kvtml;

public class QuestionAnswerActivity extends TestActivity
{
	protected int mTestType = TEST_QA;

	private EditText mInput;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mInput = (EditText) findViewById(R.id.edit_question);
		mInput.setOnEditorActionListener(new OnEditorActionListener()
		{
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
			{
				if (actionId == EditorInfo.IME_ACTION_DONE)
				{
					submit();
					return true;
				}
				return false;
			}
		});

		Button btn = (Button) findViewById(R.id.button_go);
		btn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				submit();
			}
		});

		// force show virtual input in portrait mode
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
		{
			TimerTask task = new TimerTask()
			{
				@Override
				public void run()
				{
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.showSoftInput(mInput, InputMethodManager.SHOW_IMPLICIT);
				}
			};

			Timer timer = new Timer();
			timer.schedule(task, 100);
		}

		restart();
	}

	private void submit()
	{
		Kvtml.Entry solution = mQuestion.getSolution();
		String value = mInput.getEditableText().toString();
		String right = solution.translations.get(Config.getAnswerLangId()).text;
		String msg = "";

		if (value.equals(right))
		{
			msg = "Yeah!";
			correct(solution);
			updateQuestion();
		} else
		{
			msg = "Ooops! ;(";
			mistake(solution);
		}

		Toast.makeText(QuestionAnswerActivity.this, msg, Toast.LENGTH_LONG)
			.show();
	}

	@Override
	protected void loadContentView()
	{
		setContentView(R.layout.activity_question_answer);
	}

	@Override
	protected void updateQuestion(boolean configChanged)
	{
		super.updateQuestion(configChanged);
		mInput.setText("");
	}
}