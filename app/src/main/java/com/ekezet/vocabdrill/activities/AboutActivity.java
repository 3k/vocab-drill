package com.ekezet.vocabdrill.activities;

import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.helpers.AssetsUtils;

public class AboutActivity extends Activity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);

		String txt = "";
		try
		{
			txt = AssetsUtils.readText(this, "about.html");
		}
		catch (IOException e)
		{
			e.printStackTrace();
			finish();
			return;
		}
		TextView tv = (TextView) findViewById(R.id.text_version);
		tv.setText(String.format("version %s (%d)", Config.getVersionName(), Config.getVersionCode()));
		tv = (TextView) findViewById(R.id.text_about);
		tv.setText(Html.fromHtml(txt));
		tv.setMovementMethod(LinkMovementMethod.getInstance());
	}
}