package com.ekezet.vocabdrill.activities.test_types;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.Question;
import com.ekezet.vocabdrill.QuestionFactory;
import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.activities.FilterActivity;
import com.ekezet.vocabdrill.dialogs.EntryBrowserDialog;
import com.ekezet.vocabdrill.helpers.EntriesFilter;
import com.ekezet.vocabdrill.helpers.Gradient;
import com.ekezet.vocabdrill.helpers.StatsOpenHelper;
import com.ekezet.vocabdrill.kvtml.Kvtml;

public abstract class TestActivity extends Activity
{
	public class TestStateHolder
	{
		public HashMap<String, Kvtml.Entry> mistakes = null;
		public HashMap<String, Kvtml.Entry> questionData = null;

		public EntriesFilter mFilter = null;

		public Question question = null;

		public int mistakesNum = 0;
		public int maxQuestions;
		public int maxItems = 1;
		public int entriesLeft;
	}

	public static final int LANGUAGE_DIALOG = 1000;
	public static final int ANSWER_LANGUAGE_DIALOG = 1001;
	public static final int QUESTION_LANGUAGE_DIALOG = 1002;
	public static final int EXIT_CONFIRM_DIALOG = 2000;

	public static final int TEST_CHOICE = 1;
	public static final int TEST_QA = 2;
	public static final int TEST_FLASHCARD = 3;

	/**
	 * TextView for displaying the current language of questions/answers.
	 */
	protected TextView mLangText = null;

	/**
	 * TextView for displaying the current question.
	 */
	protected TextView mQuestionText = null;

	/*
	 * Properties related to test progress.
	 */
	protected QuestionFactory mQuestionFactory = null;
	protected Question mQuestion = null;
	protected int mMaxQuestions;
	protected int mMaxItems = 1;
	protected int mEntriesLeft;

	/**
	 * String for displaying the number of mistakes (comes from resources).
	 */
	protected String mMistakesText = null;
	protected String mErrorNoTranslationText = null;

	protected int mMistakesNum = 0;
	protected HashMap<String, Kvtml.Entry> mMistakes = null;
	protected HashMap<String, Kvtml.Entry> mQuestionData = null;

	/**
	 * Internal flag for extending classes to keep track of current test type.
	 *
	 * See TEST_* constants.
	 */
	protected int mTestMode = -1;

	/**
	 * SQLite access helper.
	 */
	private StatsOpenHelper mStats;

	/**
	 * Identifier of the current KVTML file in the "files" SQL table.
	 */
	private long mFileId = -1;

	/**
	 * Handle for the Randomize menu item. This is used to manipulate the menu at
	 * runtime.
	 */
	protected MenuItem mRandomizeMenu = null;

	/**
	 * The filter for this session
	 */
	protected EntriesFilter mFilter;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		mQuestionFactory = new QuestionFactory(Config.lastData.entries);
		mStats = StatsOpenHelper.getInstance(this, mTestMode);
		mFileId = mStats.getFileId(Config.inputFile.getName());
		// prepare activity window
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			WindowManager.LayoutParams.FLAG_FULLSCREEN);
		loadContentView();
		// preload some resources
		mMistakesText = getResources().getString(R.string.mistakes);
		mErrorNoTranslationText = getResources().getString(R.string.error_no_translation_for_entry);
		// get handles for widgets
		mLangText = (TextView) findViewById(R.id.text_language);
		mQuestionText = (TextView) findViewById(R.id.text_question);
		// prepare colours
		ArrayList<View> lst = new ArrayList<View>();
		lst.add(mLangText);
		lst.add(mQuestionText);
		Gradient.colorize(lst);
		// prepare questions
		mMistakes = new HashMap<String, Kvtml.Entry>();
		// get filter
		Intent i = getIntent();
		if (i.hasExtra(FilterActivity.EXTRA_FILTER))
			mFilter = (EntriesFilter) i
				.getSerializableExtra(FilterActivity.EXTRA_FILTER);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
		super.onWindowFocusChanged(hasFocus);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(getMenuResource(), menu);
		// if there's only one language defined
		if (Config.lastData.identifiers.size() == 1)
		{
			// hide the menu
			MenuItem item = menu.findItem(R.id.option_swap_languages);
			item.setVisible(false);
			return true;
		}
		// if there are more than two languages defined
		if (2 < Config.lastData.identifiers.size())
		{
			// modify the menu
			MenuItem item = menu.findItem(R.id.option_swap_languages);
			item.setVisible(false);
			item = menu.findItem(R.id.option_change_answer_lang);
			item.setVisible(true);
			item = menu.findItem(R.id.option_change_question_lang);
			item.setVisible(true);
			mRandomizeMenu = menu.findItem(R.id.option_randomize);
			mRandomizeMenu.setVisible(true);
			mRandomizeMenu.setChecked(Config.randomize);
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	protected int getMenuResource()
	{
		return R.menu.test_menu;
	}

	@Override
	protected Dialog onCreateDialog(int id)
	{
		Dialog dialog = null;
		final Object[] idIds = Config.lastData.identifiers.keySet().toArray();
		if (idIds.length < 2)
			return null;
		switch (id)
		{
			case LANGUAGE_DIALOG:
				dialog = showLanguageDialog(idIds);
				break;
			case QUESTION_LANGUAGE_DIALOG:
				dialog = showQuestionLanguageDialog(idIds);
				break;
			case ANSWER_LANGUAGE_DIALOG:
				dialog = showAnswerLanguageDialog(idIds);
				break;
		}
		return dialog;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		if (mRandomizeMenu != null)
		{
			mRandomizeMenu.setChecked(Config.randomize);
			Resources res = getResources();
			mRandomizeMenu.setTitle(res.getString(R.string.menu_randomize) + " "
				+ (Config.randomize ? "off" : "on"));
			return true;
		}
		return super.onPrepareOptionsMenu(menu);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.option_swap_languages:
				showDialog(LANGUAGE_DIALOG);
				return true;

			case R.id.option_change_answer_lang:
				showDialog(ANSWER_LANGUAGE_DIALOG);
				return true;

			case R.id.option_change_question_lang:
				showDialog(QUESTION_LANGUAGE_DIALOG);
				return true;

			case R.id.option_restart:
				restart();
				return true;

			case R.id.option_randomize:
				final MenuItem menuItem = item;
				new AlertDialog.Builder(TestActivity.this)
					.setTitle(R.string.menu_randomize)
					.setMessage(R.string.message_swap_languages)
					.setCancelable(false)
					.setPositiveButton(R.string.button_yes,
						new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int id)
							{
								Config.randomize = !menuItem.isChecked();
								resetTest();
								updateQuestion();
							}
						})
					.setNegativeButton(R.string.button_no,
						new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int id)
							{
								dialog.cancel();
							}
						}).create().show();
				return true;

			case R.id.option_show_solution:
				Kvtml.Entry entry = Config.lastData.entries.get(mQuestion
					.getSolutionEntryId());
				Toast.makeText(this,
					entry.translations.get(Config.getAnswerLangId()).text,
					Toast.LENGTH_LONG).show();
				return true;

			case R.id.option_show_mistakes:
				if (mMistakesNum == 0)
					return false;
				EntryBrowserDialog.data = Config.lastData.cloneFromEntries(mMistakes);
				Intent i = new Intent(this, EntryBrowserDialog.class);
				// do not sort entries alphabetically
				i.putExtra("doSort", false);
				i.putExtra("windowTitle", getResources().getString(R.string.title_show_mistakes));
				startActivity(i);
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * @return a string to be set as title
	 */
	protected String getTitleString()
	{
		double percent = (Double.valueOf(mMistakesNum) / Double
			.valueOf(mMaxQuestions)) * 100.0;
		if (percent != percent)
			percent = 0;
		Log.i(getClass().getSimpleName(), String.format("question #%d", 1 + (mMaxQuestions - mEntriesLeft)));
		return String.format("%d/%d (%s: %d; %.2f%%)",
			1 + (mMaxQuestions - mEntriesLeft), mMaxQuestions, mMistakesText,
			mMistakesNum, percent);
	}

	@SuppressLint("DefaultLocale")
	protected void showFinishDialog()
	{
		if (mMistakesNum != 0)
		{
			EntryBrowserDialog.data = Config.lastData.cloneFromEntries(mMistakes);
			Intent i = new Intent(TestActivity.this, EntryBrowserDialog.class);
			i.putExtra("windowTitle", getResources().getString(R.string.title_show_mistakes));
			startActivity(i);
		}

		if (!Config.confirmRestart)
			return;

		new AlertDialog.Builder(TestActivity.this)
			.setMessage(R.string.message_test_finished)
			.setTitle(R.string.title_test_ended)
			.setCancelable(false)
			.setPositiveButton(R.string.button_yes,
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int id)
					{
						restart();
					}
				})
			.setNegativeButton(R.string.button_no,
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int id)
					{
						TestActivity.this.finish();
					}
				}).create().show();
	}

	protected Dialog showQuestionLanguageDialog(final Object[] idIds)
	{
		final CharSequence[] qitems = new CharSequence[idIds.length];
		for (int i = 0, mi = idIds.length; i < mi; i++)
		{
			qitems[i] = Config.lastData.identifiers.get(idIds[i]).name;
		}

		int checked = 0;
		String qid = Config.getQuestionLangId();
		for (int i = 0; i < idIds.length; i++)
		{
			if (String.valueOf(idIds[i]).equals(qid))
			{
				checked = i;
				break;
			}
		}

		Dialog dialog = new AlertDialog.Builder(this).setSingleChoiceItems(qitems,
			checked, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					final int selected = which;
					new AlertDialog.Builder(TestActivity.this)
						.setTitle(R.string.menu_change_question_lang)
						.setMessage(R.string.message_swap_languages)
						.setCancelable(false)
						.setPositiveButton(R.string.button_yes,
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog, int id)
								{
									Config.setQuestionLangId(String.valueOf(idIds[selected]));
									resetTest();
									updateQuestion();
								}
							})
						.setNegativeButton(R.string.button_no,
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog, int id)
								{
									dialog.cancel();
								}
							}).create().show();
					dialog.dismiss();
				}
			}).create();
		return dialog;
	}

	protected Dialog showAnswerLanguageDialog(final Object[] idIds)
	{
		final CharSequence[] aitems = new CharSequence[idIds.length];
		for (int i = 0, mi = idIds.length; i < mi; i++)
		{
			aitems[i] = Config.lastData.identifiers.get(idIds[i]).name;
		}

		int checked = 0;
		String aid = Config.getAnswerLangId();
		for (int i = 0; i < idIds.length; i++)
		{
			if (String.valueOf(idIds[i]).equals(aid))
			{
				checked = i;
				break;
			}
		}

		Dialog dialog = new AlertDialog.Builder(this).setSingleChoiceItems(aitems,
			checked, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					final int selected = which;
					(new AlertDialog.Builder(TestActivity.this))
						.setTitle(R.string.menu_change_answer_lang)
						.setMessage(R.string.message_swap_languages)
						.setCancelable(false)
						.setPositiveButton(R.string.button_yes,
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog, int id)
								{
									Config.setAnswerLangId(String.valueOf(idIds[selected]));
									resetTest();
									updateQuestion();
								}
							})
						.setNegativeButton(R.string.button_no,
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog, int id)
								{
									dialog.cancel();
								}
							}).create().show();
					dialog.dismiss();
				}
			}).create();
		return dialog;
	}

	protected Dialog showLanguageDialog(final Object[] idIds)
	{
		final CharSequence[] langItems = {
			Config.lastData.identifiers.get(idIds[0]).name,
			Config.lastData.identifiers.get(idIds[1]).name, "Random" };

		int checked = 0;
		if (Config.randomize)
			checked = 2;
		else if (Config.getQuestionLangId().equals(idIds[1]))
			checked = 1;

		Dialog dialog = new AlertDialog.Builder(this).setSingleChoiceItems(langItems,
			checked, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					final int selected = which;
					new AlertDialog.Builder(TestActivity.this)
						.setTitle(R.string.menu_swap_languages)
						.setMessage(R.string.message_swap_languages)
						.setCancelable(false)
						.setPositiveButton(R.string.button_yes,
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog, int id)
								{
									if (selected == 2)
										Config.randomize = true;
									else
									{
										Config.randomize = false;
										if (selected == 0)
										{
											Config.setQuestionLangId(String.valueOf(idIds[0]));
											Config.setAnswerLangId(String.valueOf(idIds[1]));
										} else
										{
											Config.setQuestionLangId(String.valueOf(idIds[1]));
											Config.setAnswerLangId(String.valueOf(idIds[0]));
										}
									}
									resetTest();
									updateQuestion();
								}
							})
						.setNegativeButton(R.string.button_no,
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog, int id)
								{
									dialog.cancel();
								}
							}).create().show();
					dialog.dismiss();
				}
			}).create();
		return dialog;
	}

	/**
	 * Increases the number of mistakes and saves the mistaken entry.
	 *
	 * All extending test activities should use this method for registering
	 * mistakes.
	 *
	 * @param entry The mistaken entry
	 */
	protected void mistake(Kvtml.Entry entry)
	{
		mMistakes.put(entry.id, entry);
		mMistakesNum++;
		mStats.addMistake(mFileId, entry.id, Config.getQuestionLangId(), mTestMode);

		if (mFilter != null && mFilter.hasFilter(EntriesFilter.FILTER_LEITNER))
		{
			entry.errorCount++;
			entry.currentGrade = 0;
			SimpleDateFormat df = new SimpleDateFormat(Kvtml.GRADE_DATE_DATE_FORMAT,
				Locale.getDefault());
			entry.date = df.format(Calendar.getInstance().getTime());
		}
	}

	/**
	 * All extending test activities should use this method for registering
	 * correct answers.
	 *
	 * @param entry
	 * @return Returns False if no more questions left.
	 */
	protected boolean correct(Kvtml.Entry entry)
	{
		if (mFilter != null && mFilter.hasFilter(EntriesFilter.FILTER_LEITNER))
		{
			entry.count++;
			SimpleDateFormat df = new SimpleDateFormat(Kvtml.GRADE_DATE_DATE_FORMAT,
				Locale.getDefault());
			entry.date = df.format(Calendar.getInstance().getTime());
			if (entry.currentGrade < 7)
				entry.currentGrade++;
		}
		mQuestionData.remove(entry.id);
		mEntriesLeft--;
		if (mEntriesLeft == 0)
		{
			showFinishDialog();
			return false;
		}
		return true;
	}

	/**
	 * provides a question for updateQuestion
	 *
	 * @param entries Entries of the vocabulary data set
	 * @param maxItems mMaxItems for MultipleChoice
	 * @return
	 */
	protected Question getNewQuestion(HashMap<String, Kvtml.Entry> entries, int maxItems)
	{
		return mQuestionFactory.create(entries, maxItems);
	}

	/**
	 * Prepares a new question and updates the UI.
	 *
	 * @param configChanged
	 */
	protected void updateQuestion(boolean configChanged)
	{
		mQuestion = !configChanged ? getNewQuestion(mQuestionData, mMaxItems) : mQuestion;
		if (mQuestion == null)
		{
			finish();
			return;
		}
		// set language ids
		String questionLangId = Config.getQuestionLangId();
		String answerLangId = Config.getAnswerLangId();
		if (Config.randomize)
		{
			Config.swapLanguages();
			questionLangId = Config.getQuestionLangId();
			answerLangId = Config.getAnswerLangId();
		}
		// show language settings information
		final String random = getResources().getString(R.string.random);
		final String langString = String.format("%s? <b color='white'>&raquo;</b> %s.%s",
			Config.lastData.identifiers.get(questionLangId).name,
			Config.lastData.identifiers.get(answerLangId).name,
			Config.randomize ? " (" + random + ")" : "");
		mLangText.setText(Html.fromHtml(langString));
		// display question
		Kvtml.Entry solution = mQuestion.getSolution();
		Kvtml.Translation transQuestion = solution.translations
			.get(questionLangId);
		// update the UI
		mQuestionText.setText(transQuestion == null ? mErrorNoTranslationText : transQuestion.text);
		// set the title
		setTitle(getTitleString());
	}

	protected void updateQuestion()
	{
		updateQuestion(false);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (!Config.confirmExit)
			return super.onKeyDown(keyCode, event);

		/*
		 * Ask the user if she really wants to quit the test.
		 *
		 * This is handy since some ppl may think that pressing the back button
		 * will take them back to the previous question.
		 */
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)
		{
			new AlertDialog.Builder(this)
				.setTitle(R.string.title_really_quit)
				.setMessage(R.string.message_really_quit)
				.setPositiveButton(R.string.button_yes,
					new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which)
						{
							finish();
						}
					}).setNegativeButton(R.string.button_no, null).show();
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public Object onRetainNonConfigurationInstance()
	{
		TestStateHolder obj = new TestStateHolder();
		obj.entriesLeft = mEntriesLeft;
		obj.maxItems = mMaxItems;
		obj.maxQuestions = mMaxQuestions;
		obj.mistakes = mMistakes;
		obj.mistakesNum = mMistakesNum;
		obj.question = mQuestion;
		obj.questionData = mQuestionData;
		obj.mFilter = mFilter;
		return obj;
	}

	/**
	 * Restore test state upon configuration change.
	 *
	 * @return True if state has been restored.
	 */
	@SuppressWarnings("deprecation")
	protected final boolean restoreTest()
	{
		TestStateHolder obj = (TestStateHolder) getLastNonConfigurationInstance();
		if (obj == null)
			return false;
		mEntriesLeft = obj.entriesLeft;
		mMaxItems = obj.maxItems;
		mMaxQuestions = obj.maxQuestions;
		mMistakes = obj.mistakes;
		mMistakesNum = obj.mistakesNum;
		mQuestion = obj.question;
		mQuestionData = obj.questionData;
		mFilter = obj.mFilter;
		return true;
	}

	/**
	 * Resets the test.
	 *
	 * @return True if the configuration changed.
	 */
	@SuppressWarnings("unchecked")
	private final boolean resetTest()
	{
		if (restoreTest())
			return true;
		// apply filters
		HashMap<String, Kvtml.Entry> entries = (HashMap<String, Kvtml.Entry>) Config.lastData.entries.clone();
		mQuestionData = mFilter == null ? entries : mFilter.filter(entries);
		mMaxQuestions = mQuestionData.size();
		if (mMaxQuestions == 0)
		{
			Toast.makeText(this, R.string.message_no_entries_using_filters,
				Toast.LENGTH_LONG).show();
			this.finish();
			// make sure that getQuestion is not triggered
			return true;
		}
		// resetting other state variables
		mEntriesLeft = mMaxQuestions;
		mMistakesNum = 0;
		mMistakes.clear();
		return false;
	}

	/**
	 * Resets the test and updates the UI.
	 */
	protected void restart()
	{
		updateQuestion(resetTest());
	}

	/**
	 * Sets the content view for the current test.
	 */
	protected abstract void loadContentView();
}