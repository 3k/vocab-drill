package com.ekezet.vocabdrill.activities;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.helpers.Gradient;

import java.util.ArrayList;
import java.util.List;

public class BaseMenuActivity extends AppCompatActivity
{
	protected LinearLayout mMenuLayout = null;

	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
		if (hasFocus)
			toggleMenuButtons();
	}

	protected void updateMenuColours()
	{
		if (mMenuLayout == null)
		{
			mMenuLayout = (LinearLayout) findViewById(R.id.menu_layout);
			// if onCreate() wasn't called yet
			if (mMenuLayout == null)
				return;
		}
		// update menu colours
		List<View> views = new ArrayList<>();
		View child;
		for (int i = 0, I = mMenuLayout.getChildCount(); i < I; i++)
		{
			child = mMenuLayout.getChildAt(i);
			if (child.getVisibility() == View.VISIBLE)
				views.add(child);
		}
		Gradient.colorize(views);
	}

	/**
	 * Show/hide menu buttons.
	 */
	protected void toggleMenuButtons()
	{
		updateMenuColours();
	}
}
