package com.ekezet.vocabdrill.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.ekezet.vocabdrill.R;

public class SeekBarPreference extends DialogPreference implements OnSeekBarChangeListener
{
	private static final String PREFERENCE_NS =
		"http://schemas.android.com/apk/res/com.ekezet.vocabdrill";
	private static final String ANDROID_NS = "http://schemas.android.com/apk/res/android";

	private static final String ATTR_DEFAULT_VALUE = "defaultValue";
	private static final String ATTR_MIN_VALUE = "minValue";
	private static final String ATTR_MAX_VALUE = "maxValue";

	private SeekBar mSeekBar = null;

	private TextView mValueText = null;
	private String mSummaryText = null;
	private int mMinValue = 0;
	private int mMaxValue = 10;
	private int mProgress = mMinValue;
	private int mDefaultValue = mMinValue;

	public SeekBarPreference(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		mMinValue = attrs.getAttributeIntValue(PREFERENCE_NS, ATTR_MIN_VALUE, mMinValue);
		mMaxValue = attrs.getAttributeIntValue(PREFERENCE_NS, ATTR_MAX_VALUE, mMaxValue);
		mProgress = attrs.getAttributeIntValue(ANDROID_NS, ATTR_DEFAULT_VALUE, getPersistedInt(mDefaultValue));
		setPersistent(true);
	}

	@Override
	protected View onCreateDialogView()
	{
		LayoutInflater inflater =
			(LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View dlg = inflater.inflate(R.layout.dialog_seekbar_preference, null);

		mValueText = (TextView) dlg.findViewById(R.id.seek_preference_text);
		mValueText.setText(String.valueOf(mProgress));
		mSeekBar = (SeekBar) dlg.findViewById(R.id.seek_preference_bar);
		mSeekBar.setMax(mMaxValue - mMinValue);
		// -mMinValue because onProgressChanged is triggered
		mSeekBar.setProgress(mProgress - mMinValue);
		mSeekBar.setOnSeekBarChangeListener(this);

		return dlg;
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
	{
		mProgress = progress + mMinValue;
		mValueText.setText(String.valueOf(mProgress));
		callChangeListener(mProgress);
	}

	public void setSummaryPrefixText(int res)
	{
		mSummaryText = getContext().getResources().getString(res);
	}

	@Override
	public void setDefaultValue(Object defaultValue)
	{
		if (defaultValue == null)
			return;
		mProgress = mDefaultValue = (Integer) defaultValue;
		persistInt(mProgress);
		if (mSeekBar != null)
			mSeekBar.setProgress(mDefaultValue);
	}

	@Override
	protected void onDialogClosed(boolean positiveResult)
	{
		super.onDialogClosed(positiveResult);
		if (!positiveResult)
			return;
		if (shouldPersist())
			persistInt(mProgress);
		notifyChanged();
	}

	@Override
	public CharSequence getSummary()
	{
		return String.format("%s %s", mSummaryText, mProgress);
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar)
	{
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar)
	{
	}

	@Override
	protected Object onGetDefaultValue(TypedArray a, int index)
	{
		return Integer.valueOf(mDefaultValue);
	}
}