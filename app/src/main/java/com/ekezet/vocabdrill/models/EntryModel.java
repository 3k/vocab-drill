package com.ekezet.vocabdrill.models;

import android.content.ContentValues;
import android.database.Cursor;


public class EntryModel extends BaseModel
{
	public static final String[] fields = new String[] { "_id", "file_id",
		"entry_id", "translation_id", "mistaken", "test_mode" };
	
	public static final String CREATE_STATEMENT = "CREATE TABLE entries (_id INTEGER PRIMARY KEY, file_id NUMERIC, entry_id TEXT, translation_id TEXT, mistaken NUMERIC, test_mode NUMERIC)";
	
	private long mFileId = -1;
	private String mEntryId = null;
	private String mTranslationId = null;
	private int mMistaken = -1;
	private int mTestMode = -1;
	
	public EntryModel()
	{
		super();
	}
	
	public EntryModel(long id, long fileId, String entryId, String translationId,
		int mistaken, int testMode)
	{
		super(id);
		
		setFileId(fileId);
		setEntryId(entryId);
		setTranslationId(translationId);
		setMistaken(mistaken);
		setTestMode(testMode);
	}
	
	public long getFileId()
	{
		return mFileId;
	}
	
	public void setFileId(long fileId)
	{
		mFileId = fileId;
	}
	
	public String getEntryId()
	{
		return mEntryId;
	}
	
	public void setEntryId(String entryId)
	{
		mEntryId = entryId;
	}
	
	public String getTranslationId()
	{
		return mTranslationId;
	}
	
	public void setTranslationId(String translationId)
	{
		mTranslationId = translationId;
	}
	
	public int getMistaken()
	{
		return mMistaken;
	}
	
	public void setMistaken(int mistaken)
	{
		mMistaken = mistaken;
	}
	
	@Override
	public ContentValues getValues(boolean includeId)
	{
		ContentValues values = super.getValues(includeId);
		values.put("file_id", getFileId());
		values.put("entry_id", getEntryId());
		values.put("translation_id", getTranslationId());
		values.put("mistaken", getMistaken());
		values.put("test_mode", getTestMode());
		return values;
	}
	
	@Override
	public EntryModel populate(Cursor data, boolean idIncluded)
	{
		super.populate(data, idIncluded);
		
		int start = 1;
		if (idIncluded)
		{
			setId(data.getLong(0));
			start = 0;
		}
		
		setFileId(data.getLong(1 - start));
		setEntryId(data.getString(2 - start));
		setTranslationId(data.getString(3 - start));
		setMistaken(data.getInt(4 - start));
		setTestMode(data.getInt(5 - start));
		
		return this;
	}
	
	public int getTestMode()
	{
		return mTestMode;
	}
	
	public void setTestMode(int testMode)
	{
		mTestMode = testMode;
	}
}
